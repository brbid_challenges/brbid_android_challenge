# Android Challenge

## Considerações Gerais

Você deverá fazer um fork deste projeto, todos os seus commits devem estar registrados nele, pois queremos ver como você trabalha.

## O desafio

Seu desafio será criar um CRUD usando o banco de dados do celular.


Você deve desenvolver uma tela inicial para cadastrar os seguintes dados: 

- Modelo do veículo

- Marca do veículo

- Placa do veículo

- Data da Compra 



Usa o modelo abaixo como base: 

![](https://i1.wp.com/mobiledevhub.com/wp-content/uploads/2017/11/Screenshot_1510261467.png?resize=360%2C600)


Depois você deve criar uma lista com esses cadastros usando RecycleView.
 Os itens desta lista deverão ter a opção para editar, excluir e ver detalhes.



Observações: 

- Você pode utilizar as libs que achar necessário, apenas descreva no README as foram usadas.

- A persitência no banco deverá ser utilizando Room.

- A linguagem para realização do desafio deve ser em Kotlin.

- Api mínima: 23.


## Como entregar?

* Faça um fork deste projeto e nos envie o seu link